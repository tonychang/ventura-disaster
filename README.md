# Spatial Summary of Ventura 2017-2018 Disaster (Thomas Fire and Landslides)

![NASA/ESA](http://static3.businessinsider.com/image/5a2ecd0ea3b47488378b4ee9-1200/shortly-after-the-fires-started-satellites-passing-over-southern-california-began-watching-the-blazes-develop.jpg)
**Figure 1. Thomas Fire from Sentinel-2**
# Problem Statement
On December 4, 2017 - January 14, 2018 Ventura county experienced the [largest fire][1] seen in California history. With a total cost of approximately [$177 million dollars][2], the [Thomas Fire][3] consumed 281,893 acres, destroying 1063 structures and taking 2 lives.

Shortly following the fire, winter rains of about [5 inches][4] fell on the burned landscapes. Without vegetation available to stabilize the soil, a series of mudslides added more damage to the people and property of the region. 


# Objective
This repo is an effort by [Conservation Science Partners][5] to provide a spatial history of the change in forest cover, burn area, and landslide area over the course of the entire disaster event. It is hoped that this repo serves as a resource for demonstration of utilizing cutting edge spatial analysis methods and modern remotely sensed datasets.
 
![before-after](https://static01.nyt.com/newsgraphics/2018/01/13/california-mudslides/f21f6bec73fe1768efb481555f424b7a10763506/beforeafter-Artboard_1_copy_3.jpg)
**Figure 2. Before and after images of landslide area from Digital Globe satellite imagery.**

# Task List
- [ ] Data Acquisition
    - [ ] Download Ventura County shape area
    - [ ] Download Thomas Fire Boundary area
    - [ ] Download Landsat 8 data
    - [ ] Download Sentinel 2 data
    - [ ] Download Sentinel 1 data
- [ ] Data Preprocessing
    - [ ] Calculate NDVI from Landsat/Sentinel
    - [ ] Calculate NBRI from Landsat/Sentinel
    - [ ] Prepare training data for landslide classification
- [ ] Landslide Modeling
    - [ ] Build Convolutional Neural Network for Semantic Segmentation 
- [ ] Prediction and Post-processing
    - [ ] Estimate Forest Area from Landsat/Sentinel
    - [ ] Estimate Burn Area from Normalize Burn Index
    - [ ] Estimate Landslide area from modeling

# Data Descriptions
Spatial extent:

- [Ventura County](http://www.ventura.org/gis-mapping/gis-data-downloads-mapping-base)
- [Thomas Fire](https://rmgsc.cr.usgs.gov/outgoing/GeoMAC/2017_fire_data/California/Thomas/)

Temporal extent:
- November 1, 2017 - Present

Datasets:
- [Landsat 8](https://developers.google.com/earth-engine/landsat)
- [Sentinel 1 and 2](https://cloud.google.com/storage/docs/public-datasets/sentinel-2)
- [Digital Globe](https://www.digitalglobe.com/opendata)

[1]: https://en.wikipedia.org/wiki/List_of_California_wildfires
[2]: http://www.cnn.com/2018/01/12/us/thomas-fire-california-contained/index.html
[3]: http://cdfdata.fire.ca.gov/incidents/incidents_details_info?incident_id=1922j
[4]: http://www.newsweek.com/california-fires-wiped-out-forests-and-now-huge-mudslides-are-killing-people-776676
[5]: http://www.csp-inc.org/
