**Conservation Science Partners: Criteria to evaluate whether to pursue an opportunity**

<ins>Overall assessment:</ins> On balance, this would be:

- [ ] Top priority or dream project for creativity, diversity, or reach
- [x] New, energizing, inspiring
- [ ] Satifying, but routine
- [ ] Routine and uninspiring
- [ ] Draining or dreaded 

**Trade-offs** 
Hope that the investment time of each member in project does not go over 4 hours individually.

**Business criteria**

- [x] No funding currently
- [ ] Funding under investigation
    - What is the ceiling amount of funding? What amount of funding are we likely to obtain?
    - When would funding become available?
    - Have we worked for this client before?
    - If not, do we have any connection to this client? Or would this proposal be 'cold'?
    - What would the LOE be on the proposal?
    - Do we have existing capacity to perform the work? Or would we have to bring on new staff/contractors/team members?
    - Who else is likely to bid on this (i.e., who would the competition be)?
    - What would our unique contribution/angle be to a proposal? The 'hook' that no one else could offer?

Bottomline: Is this work important from a business perspective? If so, why?

Uncertain regarding the business potential of this project. Currently under investigation as a prototype and a public service.

**Values criteria**

- [ ] Work supports our mission
- [ ] Work supports collective values, such as creativity, innovation, advancing diversity, etc. (See below.)
- [x] Work has personal significance to the proposal lead or team (e.g. field work, international, confluence of art and science, something about snowboarding)
- [x] Work strengthens internal capacity Technical, social, energetic, imaginal, etc. 
- [x] Work is a prototype that could be expanded
- [ ] Work addresses root causes of social-ecological loss If so, how
- [x] Work is cutting edge/bleeding edge
- [ ] Work is courageous

**Dimensions of Diversity, Equity, and Inclusion**

- [ ] Co-creation with new partners - expanding facets of diversity, increasing equity and inclusion
- [ ] Hiring new staff 
- [ ] Training or mentorship opportunities
- [ ] New relationships or networks in support role
- [x] Geographic scope or contribution
- [ ] New knowledge bases or cultural practices incorporated
 
**Other Dimensions of Creativity and Innovation**

- [ ] Multi/inter/trans-disciplinary  
- [x] Technical/Analytic 
    - Skills: Machine Learning
    - Tools: Google Earth Engine, Google Cloud Platform, Python, GDAL
    - Platforms: Linux OS
    - Data sources (received): Google Earth Engine, Digital Globe, USGS 
    - Data sources (produced):
    - Data visualization:
    - Figures: 
    - Web apps: 
    - Other: 

**Process**

- [ ] Innovative approaches 
- [ ] Dimensions of user-centered design  
- [ ] Facilitative approaches  
- [ ] Transformative design elements  

Bottomline: Does this work advance CSP’s diversity, creativity, or innovation goals? If so, how? 

This project examines the potential use of Sentinel 1 and 2 from the ESA, which many of the staff have not explored. 


